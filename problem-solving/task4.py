import math

n=int(input("Enter n"))
d=int(input("Enter d"))

x=math.log(d,2)
y=math.log(n,d)

if (isinstance(x, float) and x.is_integer()):
  print("d is a power of 2")
  if (isinstance(y, float) and y.is_integer()):
      print("n is a power of d")
  else:
      print("n is not a power of d")
else:
    print("d is not a power of 2")




