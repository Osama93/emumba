import math

n=int(input("Enter any number"))


def check():
    if n==1:
        print("It can be expressed as x^y with y>1 and x>0")
        print(1,"^","any_number>1")
        return True

    for x in range(2,int(n**0.5)+1,1):
        y=2
        ans=x**y
        while ans<=n and ans>0:
            if ans==n:
                print("It can be expressed as x^y with y>1 and x>0")
                print(x,"^",y)
                return True
            y+=1
            ans=x**y
    print("It cannot be expressed as x^y with y>1 and x>0")
    return False
check()



