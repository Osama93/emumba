

def countpaths(x,y):
    if x==0 or y==0:
        return 1

    return countpaths(x-1,y)+countpaths(x,y-1)

print(countpaths(3,0))
