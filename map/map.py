def fahrenheit(T):
    return ((float(9)/5)*T + 32)

def celsius(T):
     return (float(5)/9)*(T-32)

def kelvin(T):
    return (T+273.15)

temperatures = (36.5, 37, 37.5, 38, 39)
F = list(map(fahrenheit, temperatures))

C = list(map(celsius, F))
K = list(map(kelvin, C))

print("Temperatures")
print("In Farenheit: ",F)
print("In Celcius: ",C)
print("In Kelvin: ",K)

########comments
